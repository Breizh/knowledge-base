# DPKG

!!! info "read [this article](./deb.md) first !"
    DPKG lets you interact with `.deb` archives and with its content, even once unpacked.  
    Make sure to read [this article](./deb.md) which details the `.deb` archive structure if needed.
## inspection
Print control file information
```bash
dpkg [-I|--info] <ARCHIVE.deb>
```

Print files within a deb archive
```bash
dpkg [-c|--content] <ARCHIVE.deb>
```

prints file paths from an unpacked `.deb` archive by package name
```bash
dpkg [-S|--search] <PACKAGE_NAME>
```
---

Print package status
```bash
dpkg [-s|--status] <PACKAGE_NAME>
```
??? info "Package states"
    `not-installed`
        The package is not installed on your system.

    `config-files`
        Only the configuration files or the postrm script and its data exist on the system.

    `half-installed`
        Package installation has been started, but not completed for some reason.

    `unpacked`
        The package is unpacked, but not configured.

    `half-configured`
        package unpacked and configuration started but not completed for some reason.

    `triggers-awaited`
        The package awaits trigger processing by another package.

    `triggers-pending`
        The package has been triggered.

    `installed`
        The package is correctly unpacked and configured.

---

## package setup
Install a package
```bash
dpkg [-i|--install] <ARCHIVE.deb>
```
??? info "implicit installation steps:"
    1. Extract the ==control files==  of the new package.

    1. If another version of the same package was installed before the new installation,  
        ==execute prerm script== of the old package.

    1. ==Run preinst script==, if provided by the package.

    1. ==Unpack the new files==, and at the same time back up the old files,  
        so that if something goes wrong, they can be restored.

    1. If another version of the same package was installed before the new installation, ==execute the postrm script== of the old
        package. Note that this script is executed after the preinst script of the new package, because new files are written at the
        same time old files are removed.

    1. ==Configure the package== . See `--configure` for detailed information about how this is done.
Remove a package and keep "conffiles"
```bash
dpkg [-r|--remove] <PACKAGE_NAME>
```
??? info "implicit suprresion steps:"
    1. Run prerm script

    2. Remove the installed files

    3. Run postrm script

Purge a package, (remove a package & its conffiles)
```bash
dpkg [-P|--purge] <PACKAGE_NAME>
```

---

## architecture

Print architecture(s) supported by `dpkg`
```bash
dpkg --print-architectures
```

Print architecture(s) that could be supported by `dpkg` if added
```bash
dpkg --print-foreign-architectures
```

Add support for a given architecture
```bash
dpkg --add-architecture <ARCHITECTURE-NAME>
```

Remove support for a given architecture
```bash
dpkg --remove-architecture
```
---

## advanced usage
Build a package
```bash
dpkg [-b|--build] <SOURCE-FOLDER>
```
unpack a package

```bash
dpkg --unpack <PACKAGE_NAME>
```

??? question "why would i `--unpack` then `--configure` instead of `--install`ing right away ?"
    Sometimes it may be necessary to edit postinst scripts, installing right away would lead in the execution of post-inst scripts :skeleton:.



configure a package
```bash
dpkg [--configure [-a|--pending]] <PACKAGE_NAME>
```
??? info "configuration steps"
    - saves conffiles if already existing
    - unpack conffiles
    - runs postinst scripts (if provided by maintainer)

To reconfigure a package, use `dpkg-reconfigure`
```bash
dpkg-reconfigure locales
```

---

## return code
0   <=>     OK  
1   <=>     error  
2   <=>     fatal error
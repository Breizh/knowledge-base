# .deb

## structure
A `.deb` archive contains:

- a control file `control`
- a version file `debian-binary`
- the actual program (& dependencies) `data`

!!! example
    ```bash
    ralsei@DESKTOP-A9R4IN3:/tmp/stortruc$ ar -t PACKAGE_NAME.deb #-x to decompress
    debian-binary
    control.tar.xz
    data.tar.xz
    ```
---

### debian binary
The `debian-binary` file contains the .deb archive version
!!! example
    ```bash
    ralsei@DESKTOP-A9R4IN3:/tmp/stortruc$ cat debian-binary
    2.0
    ```
---

### control file

!!! info
    The control file contains meta-information regarding the program.

To print out the control file
```bash
ar -x PACKAGE_NAME.deb
tar -xvf control.tar.xz
cat control
```

Or use `apt` which is more user-friendly and formats the output
```bash
apt-cache show PACKAGE_NAME
```

#### package information
Here is a non-exhaustive list of fields the control file could contain:

- package name
- package version
- package size
- package description
- maintainer
- architecture

#### dependency fields

`Depends:`  
Required dependencies, `apt` relies on this field to install them. This field often relies on logical operators to defined valid versions of dependencies.
!!! info
    Parentheses are not supported in the `Dependency:` field Thus an indirect formualtion is regularly used.
    Instead of  A or (B and C) we must write “(A or B) and (A or C)” => `A | B, A | C`.


`Conflicts`  
Packages that cannot be installed because already installed packages would be affected.  
(e.g same port, same file path, etc...) The imcopatibility is, in most cases, not ==transitory==.

`Breaks`  
Packages that cannot be installed because already installed packages would be affected.  
Unlike the Conflicts field, the problem is in most case transitory, and can be resolved.  
This is the case mainly because imcompatible packages can most of the time be upgraded to  
resolv the imcopatibility.


`Recommends:`  
Packages that ==can improve== the program's functionality.  
However theses packages are not necessary for the program to operate.

`Suggests:`  
Packages that ==can extend== the program functionalities.  
However theses packages are not necessary for the program to operate.

`Provides:`  
The provide field, makes it so that you don't need to install the stated dependency, as an equivalent resides in the given package.  
This equivalent is called a ==virtual package==.

`Replaces:`  
Packages that can replaces already installed package's content, wihtout specifying  new packages in this section,
new packages would conflict with the one already installed.


!!! info
    Meta-packages are empty packages only containing a control file stating dependencies, not to "confondre" with virtual packages !

!!! info
    Provides Depends & Conflicts can all target a same package:
    ```bash
        Package: postfix
        Version: 2.0.0
        Depends: mail-transport-agent
        Provides: mail-transport-agent
        Conflicts: mail-transport-agent
        Description: A mail server
    ```
    The `Conflicts:` field does not account the actual package (in this case `postifx`).  
    The consequence are that:
    
    - any package Providing `mail-transport-agent` cannot be installed
    - any package requiring `mail-transport-agent` will rely on postifx's virtual package

    This makes sense, as setting up 2 mail server makes no sense, and they could hinder each other functioning.

!!! example
    ```bash
        $ apt-cache show apt
        Package: apt
        Version: 2.2.4
        Installed-Size: 4337
        Maintainer: APT Development Team <deity@lists.debian.org>
        Architecture: amd64
        Replaces: apt-transport-https (<< 1.5~alpha4~), apt-utils (<< 1.3~exp2~)
        Provides: apt-transport-https (= 2.2.4)
        Depends: adduser, gpgv | gpgv2 | gpgv1, libapt-pkg6.0 (>= 2.2.4), debian-archive-keyring, libc6 (>= 2.15), libgcc-s1 (>= 3.0), libgnutls30 (>= 3.7.0), libseccomp2 (>= 2.4.2), libstdc++6 (>= 9), libsystemd0
        Recommends: ca-certificates
        Suggests: apt-doc, aptitude | synaptic | wajig, dpkg-dev (>= 1.17.2), gnupg | gnupg2 | gnupg1, powermgmt-base
        Breaks: apt-transport-https (<< 1.5~alpha4~), apt-utils (<< 1.3~exp2~), aptitude (<< 0.8.10)
        Description-en: commandline package manager
        This package provides commandline tools for searching and
        managing as well as querying information about packages
        as a low-level access to all features of the libapt-pkg library.
        .
        These include:
        * apt-get for retrieval of packages and information about them
            from authenticated sources and for installation, upgrade and
            removal of packages together with their dependencies
        * apt-cache for querying available information about installed
            as well as installable packages
        * apt-cdrom to use removable media as a source for packages
        * apt-config as an interface to the configuration settings
        * apt-key as an interface to manage authentication keys
        Description-md5: 9fb97a88cb7383934ef963352b53b4a7
        Tag: admin::package-management, devel::lang:ruby, hardware::storage,
        hardware::storage:cd, implemented-in::c++, implemented-in::perl,
        implemented-in::ruby, interface::commandline, network::client,
        protocol::ftp, protocol::http, protocol::ipv6, role::program,
        scope::application, scope::utility, suite::debian, use::downloading,
        use::organizing, use::playing, use::searching, works-with-format::html,
        works-with::audio, works-with::software:package, works-with::text
        Section: admin
        Priority: required
        Filename: pool/main/a/apt/apt_2.2.4_amd64.deb
        Size: 1491328
        MD5sum: 24d53e8dd75095640a167f40476c0442
        SHA256: 75f07c4965ff0813f26623a1164e162538f5e94defba6961347527ed71bc4f3d
    ```
---

### data.tar.xz
The actual program and its **internal** dependencies.  

!!! example
    ```bash
    ralsei@DESKTOP-A9R4IN3:/tmp/stortruc/data$ tar -xvf data.tar.xz
    ./
    ./opt/
    ./opt/MegaRAID/
    ./opt/MegaRAID/storcli/
    ./opt/MegaRAID/storcli/storcli
    ./opt/MegaRAID/storcli/storcli64
    ./usr/
    ./usr/share/
    ./usr/share/doc/
    ./usr/share/doc/storcli/
    ./usr/share/doc/storcli/changelog.Debian.gz
    ./usr/share/doc/storcli/copyright
    ./usr/share/smartupdate/
    ./usr/share/smartupdate/storcli/
    ./usr/share/smartupdate/storcli/component.xml
    ```


# Bash

## variables
types & instanciation
```bash
#!/bin/bash
MYVAR1=23
X=1
message="Hello there"
my_list=(1, 2, 12, "hedgehog")
```

!!! warning
    no spaces around the `=` sign as:
    ```bash
    #sets the my_list ENV variable to nothing and runs (1, 2, 12, "hedgehog") as a program
    my_list= (1, 2, 12, "hedgehog")
    
    #runs my_list as a program with the argument =(1, 2, 12, "hedgehog")
    my_list =(1, 2, 12, "hedgehog")

    #runs my_list as a program and passes it two arguments, = and (1, 2, 12, "hedgehog")
    my_list = (1, 2, 12, "hedgehog")

    ```

---

interpolation

```bash
word ="Windows"
echo "Linux > ${word}"
```

---

substitution

variable can be assigned with a command output using `$(COMMAND)` or \``COMMAND`\`

```bash
#!/bin/bash
date=$(date)
echo $date;
```

---

arguments

call a script and pass it parameters
```bash
#!/bin/bash
./myscript firstarg "secondarg" $var3
```

Parameters are then accessible in the script reffering to them as `$i` `i` corresponding to the placement of the parameter in the running command.
```bash
#!/bin/bash
echo "script name is $0"
echo "the first parameter is $1"
echo "the second parameter is $2"
echo "the third & last parameter is $3"
```

---

!!! question
    - [ ] conditions
    - [ ] looping
    - [ ] functions
    - [ ] best practices
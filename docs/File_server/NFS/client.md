## Client
### installation & configuration
Install the package
```bash
apt install nfs-common
```

Create a local directory on which to map the NFS server directory
```bash
mkdir /mnt/nfs_server_folder_name
```

Manually mount the remote folder by using the `mount` utility
```bash
mount -t nfs <DNS:/PATH/TO/REMOTE/FOLDER> <PATH/TO/LOCAL/FOLDER>
```

Or mount the remote folder on boot by editing the `/etc/fstab` configuration
```bash
SERVER_IP_OR_DNS:/PATH/TO/REMOTE_FOLDER PATH_TO_LOCAL_FOLDER nfs defaults,user,auto,_netdev,bg 0 0
```

!!! danger
    omitting the `bg` keyword may get the client unable to boot if the NFS server is not accessible.  
    As fstab is executed at each boot.

### debugging

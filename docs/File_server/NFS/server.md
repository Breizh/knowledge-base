## Server
###  installation & configuration

Install the package
```bash
apt install nfs-kernel-server nfs-common
```

Configure the server by editting the `/etc/exports` file
```conf
folder ip_range(options) [ip_range(options)]
#e.g below
/home/ralsei/folder_to_share 192.168.1.0/24(rw, all_squash, anonuid=<NUMBER>, anongid=<NUMBER>, sync, no_subtree_check)
```
reload the service can be done using one of the two following commands
```bash
exportfs -rav
systemctl reload  nfs-kernel-server.service
```

### debugging

!!! tip
    `showmount -e`  
    to determine if the configuration has been applied or not.  
    If not, consider restarting the service.

!!! tip
    It is possible to secure the NFS server by relying on:

    - `/etc/hosts.allow`
    - `/etc/hosts.deny`

!!! tip
    2 other configuration files exist:

    - `/etc/defaults/nfs-common`  
    - `/etc/defaults/nfs-kernel-server`

!!! danger
    If the server goes idle, it may freeze any interaction with it.
# Flask

## project structure
Personal recommendation:

- `config.py` contains app & library configurations
- `forms.py` contains form declaration
- `model.py` contains database & tables declaration (Mvc)
- `routes.py` contains routes declaration (mvC)
- `templates/` contains web pages (mVc)
- `app.py` entry point (import * and starts the web app)
- `static/` contains static files (e.g png, pdf, css, etc...)
- `admin_views.py` contains admin panel customization
- `requirements.txt` to ease dependency installation

!!! info
    `forms.py`, `model.py` &` admin_views.py` are optional

---

## configuration
`config.py` contains app & libraries configurations.  
To create a Flask application
```python
#config.py
from flask import Flask
app = Flask(__name__)
```
You can then use various Flask functions to work on your web app.

!!! example
    - `app.route()` lets you map a function call to an URL
    - `app.run()` starts the development server & listens for incomign traffic
    - `app.request` allows you to access GET & POST variable, sent by the client
    - `app.response` the HTTP response sent to the client.

    See below for concrete usage...

Now that Flask is instanciated, you can configure it, as well as related libraries.
```python
#config.py
#setting the default language to french & the default timezone to Paris
from flask_babel import Babel
app.config['BABEL_DEFAULT_TIMEZONE'] = 'Paris'
app.config['BABEL_DEFAULT_LOCALE'] = 'fr'
babel = Babel(app) # applies configuration
```

!!! note
    Any Flask library comes with a documentation easing its deployment.  
    In this case, [documentation here](https://python-babel.github.io/flask-babel/)
    
---
    
## forms
!!! info
    `forms.py` contains form declaration.  
    In this section we discuss the use of FlaskForm library to ease form management.

To create a form, declare a class inherting FlaskForm
```python
#forms.py
class MyForm(FlaskForm):
```

You can then assign form fields to the form
```python
#forms.py
class MyForm(FlaskForm):
    name = StringField(label='name: ', validators=[DataRequired()])
    age = IntegerField(label='age: ', validators=[DataRequired()])
    email = StringField(label='e-mail address: ', validators=[DataRequired()])
    send = SubmitField(label='Send')
```
each Field is assigned to a variable.  
Fields always have `Field` as a suffix.  

- common field types are listed [here](https://wtforms.readthedocs.io/en/2.3.x/fields/#basic-fields)
- every field takes at least [theses arguments](https://wtforms.readthedocs.io/en/2.3.x/fields/#the-field-base-class)

!!! info
    To develop a minimalist form, you can stick to the label and validators arguments

Once a form declared in `forms.py` head to the [routes > form ](#routes) section, which explains:

- how to make a form available for a given URL
- how to process forms once filled by the client

---

## model
!!! info
    `model.py` contains the database instanciation & SQL table definitions.  
    In this section, we discuss the use of FlaskSQLAlchemy to ease database management.

first, instanciate `FlaskSQLAlchemy`
```python
#model.py
db = SQLAlchemy()
```

you can then create SQL tables by declaring python classes inherting the `db.Model` class
```python
#model.py
class Book(db.Model):
```

each variable reffers to a column
```python
#model.py
class Book(db.Model):
    __tablename__ = 'book'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    price =db.Column(db.Float)
```

- column types can be found [here](https://flask-sqlalchemy.palletsprojects.com/en/2.x/models)
- column arguments can be found [here](https://docs.sqlalchemy.org/en/20/core/metadata.html#sqlalchemy.schema.Column)

Once tables are declared head to the [routes > model](#routes) section, which explains:

- how to interact with the database graphically
- how to interact with the database using Python

---

## routes
### Intro
`routes.py` contains the web app routes, a route is an URL mapped to a function.  
When a user request an URL, the corresponding function is triggered.  
Usually, the web app then returns a web page.

In this example, requesting `http://<DOMAIN>/home` sends back the `home.html` template to the user
```python
#routes.py
@app.route('/home', methods=['GET'])
def accueil():
    return render_template('home.html')
```

Important to note that you can pass variables to a template and access them using jinja
=== "route"
    ```python
    #routes.py
    @app.route('/home', methods=['GET'])
    def accueil():
        return render_template('home.html', title="Welcome !")
    ```
=== "template"
    ```jinja
    <!-- templates/home.html -->
    <h1>{{title}}</h1>
    <p>lorem ...</p>
    ```

---


### form
#### rendering
!!! warning
    Before reading, go through the [forms](#forms) section

Once a form is declared, you can make it available to users
=== "route"
    ```python
        #routes.py
        @app.route('/myForm', methods=['GET'])
        def my_form():
            my_form = MyForm() #instanciate the form
            #pass the form to the template & send to the user
            return render_template('my_form.html', my_form=my_form)
    ```
=== "template"
    ```jinja
        <!-- templates/myForm.html -->
        <h1>Please fill this form !!!</h1>
        {% raw %}{{ render_form(my_form, action="/my_form",method="POST") }}{% endraw %}
    ```

#### processing

`form.validate_on_submit():` can be used to process a form once filled up & sent by the user.
```python
#routes.py
@app.route('/myForm', methods=['GET'])
def my_form():
    my_form = MyForm() # instanciate the form

    # If form correctly filled up by the user
    if form.validate_on_submit():
        # form content is logged
        logging.DEBUG(f"FORM FILLED UP !\n{form.data}")
        return redirect('/home')
    
    # pass the form to the template & send to the user
    return render_template('my_form.html', my_form=my_form)
```

!!! note
    `form.data` is used to access data input by the user.  
    In this example content is logged, but it could be stored in a DB, e-mailed, or anything else.

---

### model
!!! warning
    Before reading, go through the [model](#model) section

#### CRUD via GUI
Flask can dynamically create a CRUD interface to interact with each table declared in `model.py`.  
It is done plugging the Flask-admin library to your application and allowing each table one by one
=== "syntax"
    ```python
    #config.py
    from flask_admin.contrib.sqla import ModelView
    admin = admin = Admin(app, name='admin panel', template_mode='bootstrap4') # template_mode if using flask-bootsrap !
    admin.add_view(ModelView(model=<TABLENAME1>, session=db.session, name="TABLENAME", category="IT"))
    admin.add_view(ModelView(model=<TABLENAME2>, session=db.session, name="TABLENAME", category="IT"))
    ```
=== "example"
    ```python
    #config.py
    from flask_admin.contrib.sqla import ModelView
    admin = admin = Admin(app, name='admin panel', template_mode='bootstrap4')
    admin.add_view(ModelView(model=Book, session=db.session, name="book", category="IT"))
    ```

#### CRUD via Python
In this section, CRUD operations are demonstrated relying on the following example

!!! example
    ```python
    class IT_Equipment(db.Model):
        __tablename__ = 'it_equipment'
        id = db.Column(db.Integer, primary_key=True)
        name = db.Column(db.String(120), nullable=False, unique=True)
        price = db.Column(db.Float, nullable=False)
        picture_path = db.Column(db.String(120), nullable=True)
        pdf_path = db.Column(db.String(120), nullable=True)
        desc = db.Column(db.String(6000), nullable=True)
        equipment_type_id = db.Column(db.Integer, db.ForeignKey('equipment_type.id'))
        equipment_type = db.relationship('Equipment_type', backref='it_equipment', foreign_keys=[equipment_type_id])
        
        def __str__(self):
            return self.nom
    ```

=== "create"
    appending data in the `IT_Equipment` table
    ```python
    @app.route('/my_route')
    def my_route():
        # instantiate an object from the table type declared in `model.py`
        new_equipment = IT_Equipment(name="Dell Precision", price=777, equipment_type='laptop')
        
        # appending
        db.session.add(new_equipment)
        db.session.commit()
        
        return redirect('/home')
    ```
=== "read"
    querying data from the `IT_Equipment` table
    ```python
    @app.route('/my_route')
    def my_route():
        equipments = IT_Equipment.query.all() # a list of "IT_Equipment" objects, from the "IT_Equipment" table declared in `model.py`
        equipment = IT_Equipment.query.get(1) # a single equipment having `1` for primary key 
        bags = IT_Equipment.query.filter_by(equipment_type='bag') # get all "IT_Equipment" in DB when equipment_type='bag' is true

        # you can then interact with the object
        print("name: " + equipment.name)
        print("type: " + equipment.equipment_type)
        
        return redirect('/home')
    ```
=== "update"
    updating data from the `IT_Equipment` table
    ```python
    @app.route('/my_route')
    def my_route():
        # fetch the item
        equipment = IT_Equipment.query.get(1) # a single equipment having `1` for primary key 

        # modification
        equipment.price = 27
        
        # commiting modification(s)
        db.session.commit()

        return redirect('/home')
    ```
=== "delete"
    querying data from the `IT_Equipment` table
    ```python
    @app.route('/my_route')
    def my_route():
        # fetching
        equipment = IT_Equipment.query.filter_by(name='Dell Essential 15 (ES1520P)')
        
        # deleting
        db.session.delete(equipment)
        db.session.commit()
        
        return redirect('/accueil')
    ```

---


## templates
Templates are jinja files, as seen in the [routes](#intro) section, you can pass parameters to a template, and reffer to them in it.  
[Jinja documentation here](https://jinja.palletsprojects.com/en/3.1.x/)



## Misc
- [bootstrap theme & render_form function](https://bootstrap-flask.readthedocs.io/en/stable/index.html)
- [administrator panel to interact with table entries & static files](https://flask-admin.readthedocs.io/en/latest/index.html)
- [LDAP authentication](https://flask-ldap3-login.readthedocs.io/en/latest/)

*[SQLAlchemy]: an SQL overlay easing database interaction
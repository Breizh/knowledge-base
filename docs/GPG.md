# GnuPG 

GnuPG (also called GPG) is an implementation of the OpenPGP standard (RFC4880).  
GPG lets you encrypt decrypt and sign data to ensure integrity, confidentiality & authenticity.

!!! info
    This recap addresses <b>the use</b> of GnuPG,  
    you can still find a brief insight on how things works in [this section](#how-it-works)

## install GnuPG
```bash
sudo apt-get install gnupg
```

## manage your own keys
### generate a key pair
```bash
gpg --gen-key
```

It generates the following output
```bash
-------------------------------
pub   rsa3072 2023-07-25 [SC] [expires: 2025-07-24]
      CA4B954AA7F28F24FB4A0A0AF6CABD4D7D76BA9F
uid           [ultimate] tempkey <tempkey@gmail.com>
sub   rsa3072 2023-07-25 [E] [expires: 2025-07-24]
```

- `rsa3072` => key type: `RSA` & key size `3072` bits
- `CA4B954AA7F28F24FB4A0A0AF6CABD4D7D76BA9F` => fingerprint
- `tempkey@gmail.com` => related e-mail
- `2023-07-25` => creation date
- `2025-07-24` => expiration date
---

### list your keys
```bash
gpg --list-public-keys
gpg --list-secret-keys
```

### generate a revocation certificate
```bash
gpg --output ~/revocation.crt --gen-revoke tempkey@gmail.com
chmod 600 ~/revocation.crt
```
!!! info
    Put the certificate on the target device to make your GPG key pair unusable

## sharing keys
### import a key
#### locally
```bash
gpg --import <FILENAME>
```

#### from a key server
key servers can be used to store & access public GPG keys  
```bash
gpg --keyserver <SERVER_URL> --search-keys <NAME|MAIL_ADDRESS>
```

### export a key
#### via generated file

=== "syntax"
    ```bash
    gpg --ouptut <FILENAME> --export <YOUR_PUBKEY_ID>
    ```
=== "example"
    ```bash
    gpg --output pubkey.asc --export CA4B954AA7F28F24FB4A0A0AF6CABD4D7D76BA9F
    ```
Then send the output to the concerned persons

#### via a key server
=== "syntax"
    ```bash
    gpg --send-keys --keyserver <KEYSERVER> <KEY-ID>
    ```
=== "example"
    ```bash
    gpg --send-keys --keyserver keyring.debian.org CA4B954AA7F28F24FB4A0A0AF6CABD4D7D76BA9F
    ```
Then inform them that they can fetch your public key from the said key server.

### Sign a key
Signing a key notifies GPG that you trust the key, and that the owner is the one you think it is
```bash
gpg --sign-key email@example.com
```

## generate a signature
=== "syntax"
    ```bash
    gpg [--sign|--clearsign|--detach-sign] <CONTENT>
    ```
=== "example"
    ```bash
    gpg --clearsign file.txt # .asc (plain text)
    gpg --sign file.txt # .gpg
    gpg --detach-sign file.txt #.sig
    ```

## verify a signature
=== "syntax"
    ```bash
    gpg [--keyserver-options auto-key-retrieve ] --verify <SIGNATURE> <SIGNED_CONTENT>
    ```
=== "example"
    ```bash
    gpg --keyserver-options auto-key-retrieve --verify archlinux-2023.04.01-x86_64.iso.sig archlinux-2023.04.01-x86_64.iso
    ```
## encrypt content
To encrypt content
```bash
gpg --encrypt --sign --armor --recipient dest@gmail.com
```

!!! info
    `--armor` converts the content to ASCII to ease e-mail communications.  
    At reception the ASCII is **unarmored** back to binary data.

## decrypt content
To decrypt a message
```bash
gpg content.asc
```

## How it works

!!! info
    Signing a content (a file for example) is done by using the signatory's private key,  
    Verifying a signature is done by using the signatory's public key.



### about signatures
A signature is a form of checksum, it is calculated based on the entire content being signed.  
Any alteration made to the content will result in a different signature generation.

A signature is encrypted using the signatory's private key.  
This makes it harder for an attacker to tamper the signature.

Verifying a file using the `gpg --verify <SIGNATURE> <CONTENT>` command :

- decrypts the `SIGNATURE` using the signatory's public key
- generates a signature from the `CONTENT`
- compares the signatory's signature with the one it just generated

### about encryption & decryption

- Content is encrypted using the recipient's public key.  
- Content is decrypted by the recipient by using its private key.

!!! info
    This confirms **CONFIDENTIALITY**


Encryption ensures that only the recipient can read the content.  
BUT nothing guaranties to the recipient the sender authenticity.

This is why it is recommended for the sender to attach a content signature to the content.  
Thus, the recipient can use the sender's public key to decrypt the signature and verify it by calculating its own checksum.

!!! info
    This confirms **AUTHENTICITY & INTEGRITY**.

!!! warning
    It is important to verify that the signatory's public key belongs to the person you really think it does.  
    You can do this by asking the signatory's public key fingerprint, find it on its website, or on another trusted source...  
    You can then compare it to the fingerprint from the signature you downloaded.
    


### external documentation
- [davesteele's take on GPG](https://davesteele.github.io/gpg/2014/09/20/anatomy-of-a-gpg-key/)
- [digital ocean on GPG](https://www.digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages#verify-the-other-person-s-identity)
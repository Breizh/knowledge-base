# LDAP
## Basic LDAP Components
#### attribute
An attribute is a characteristic about an entity.  
It is represented as a `key=value` pair such as `age=21`

---

#### entry
An entry is a set of characteristics defining an entity.  
(e.g `fname=john lname=Doe age=21`)

An entry is identified by its Distinguished Name often reffered as `dn`
```bash
dn: sn=John, ou=people, dc=example, dc=com
```

---

#### ObjectClass
An ObjectClass packages sets of characteristics, you can assign an ObjectClass to an entry to give it several predefined attributes.

an ObjectClass is represented as an attribute as well, so you must assign it a value in an entry to be able to use the ObjectClass attributes.

```bash
dn: ...
ObjectClass: person
``` 
gives access to the following attributes:

- `cn`: Common name
- `description`: a description
- `SeeAlso`: related entries
- `sn`: surname
- `telephoneNumber`: A telephone number
- `userPassword`: A (hashed) password

An ObjectClass may be from one of the following types:

- ==structural==: Meaning that an entry can be created just by inherting from the Objectclass and setting its attributes.  
- ==auxiliary==: Meaning that the ObjectClass attributes are complementing an entry, the entry cannot solely rely on the ObjectClass attributes to be instanciated.  

==1== structural ObjectClass per entry  
==0-*== auxiliary ObjectClass per entry

!!! example
    The predefined `person` ObjectClass in OpenLDAP is a ==structural== ObjectClass,  
    ==an entry can be created upon it==, no more attributes are required.

    The predefined `posixAccount` ObjectClass in OpenLDAP is an ==auxiliary== ObjectClass,  
    ==an entry cannot solely rely on it== to be instanciated.

---

#### DIT
DIT refers to the structure used to organize data inside of an LDAP server.
It basically is a tree like structure. Each entry is a branch / object of that tree.


```txt
            dc=COM
              |
          dc=ORG_NAME
         /          \
   ou=people    ou=computer
       /              \
cn=John               ...
```
`dc` or Domain Components is an attribute used at the top of the DIT to define the top structure in a DNS way 
`ou` is an attribute containing the name of the Organizational Unit (it holds a group name).

Thus, you can have two similar entries having a different semantic.  
An entry is identified by its ==Distinguished Name== also called reffered as `dn`.

To get an entry's `dn`, sum up the name of each object from then entry to the root of the tree structure.
(e.g `dn: sn=John, ou=people, dc=ORG_NAME, dc=COM`)

---

### Defining LDAP components
#### attribute
=== "memo"
    ```bash 
    attributetype (<OID> [NAME name]
        [DESC description]
        [EQUALITY condition]
        [SUBSTR condition]
        [SYNTAX ?] [SINGLE-VALUE] [NO-USER-MODIFICATION] [USAGE attributeUsage])
    ```
=== "example"
    ```ldap 
    attributetype (2.5.4.41 NAME 'name'
        DESC 'rfc4519: common supertype of name attributes'
        EQUALITY caseignorematch
        SUBSTR caseignoresubstringsmatch
        SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{32768} )
    ```

!!! info
    More information about matching rules can be found [here](https://ldap.com/matching-rules/)


#### ObjectClass

=== "memo"
    ```bash
        objectclass(<OID> [NAME <name>]
            [DESC <description>] [SUP <ObjectClassName> <OIDs>]
            [STRUCTURAL|AUXILIARY|ABSTRACT])
            [MUST key1 $ key2 $ ...] #must variables must be set at entry instanciation
            [MAY key1 $ key2 $ ...] #may variables are optionaly set at entry instanciation
    ```
=== "example"
    ```bash
        objectclass( 2.5.6.7 NAME 'organizationalPerson'
        SUP person STRUCTURAL
    ```

#### Schema
Schemas are used to package ObjectClasses and attributes together.


!!! example
    ```bash
    objectclass ( 2.5.6.6 NAME 'person' DESC 'RFC2256: a person' SUP top STRUCTURAL
    MUST ( sn $ cn )
    MAY ( userPassword $ telephoneNumber $ seeAlso $ description ) )

    attributetype ( 2.5.4.4 NAME ( 'sn' 'surname' )
    DESC 'RFC2256: last (family) name(s) for which the entity is known by' SUP name )

    attributetype ( 2.5.4.4 NAME ( 'cn' 'commonName' )
    DESC 'RFC4519: common name(s) for which the entity is known by' SUP name )
    ```

## LDAP commands
=== "memo"
    === "search"
        ```bash
        ldapsearch [-x] [-H ldap|ldaps|ldapi://ldap_server.org] [-b dn] [-s sub|one|children]
        ```
    === "add"
        ```bash
        ldapadd
        ```
    === "delete"
        ```bash
        ldapdelete
        ```
=== "example"
    === "search"
        ```bash
        ldapsearch -x -s one -H ldaps://ducros_ldap.com -b ou=people,ou=building,dc=DUCROS,dc=FR
        ```
    === "add"
        ```bash
        ldapadd
        ```
    === "delete"
        ```bash
        ldapdelete
        ```
useful flags here:

- `-x` to rely on Basic Authentication (header contains basic encoded Username / Password pair)
- `-h <hostname>` to specify ldap server URL
- `-b <dn>` to specify the starting point of the search
- `-s <scope>` to specify the depth of the search
- `-W -D <USER_DN>` to login using the targeted user credential

## LDAP configuration
### client-side
### server-side

---

!!! Question
    - [x] Directory Information Tree section
    - [ ] commands & examples to interact with entries (add search modify)
    - [ ] client-side configuration
    - [ ] server-side configuration
    - [ ] commands to define schemas objectclasses and attributes  
    - [ ] move it away from the Linux folder as LDAP is also used in the "Windows world"

*[directory service]: annuaire
*[DIT]: Directory Information Tree
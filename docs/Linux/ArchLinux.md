# Arch Linux

Installing ArchLinux :smile:  
Following the installation  guide [here](https://wiki.archlinux.org/title/Installation_guide)


Download an ArchLinux ISO [here](https://archlinux.org/releng/releases/)  
it contains the kernel as some default packages

!!! note
    An ISO is a file formatted in the same way a disk would be.  
    "burning" an ISO file means copying the content of the ISO file, directly on a disk.

```bash
wget http://mirror.archlinux.ikoula.com/iso/2023.04.01/archlinux-2023.04.01-x86_64.iso
```
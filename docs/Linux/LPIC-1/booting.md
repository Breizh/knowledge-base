# 101.2 Boot the system

!!! information
    This chapter is about:
    
    - How BIOS & UEFI boot sequence works.
    - Configuring the bootloader  
    - The booting sequence  
    - Boot logs


BIOS & UEFI behave differently.

## BIOS
BIOS is a firmware that starts everytime a computer is powered on.
BIOS requires at list a disk defined in the "boot order" parameter to contain a Master Boot Record.

A Master Boot Record is the first 512 bytes of a disk, it is splitted in two parts:

- The bootloader, a program executed by BIOS to launch the kernel  & its settings into RAM (itself contains 2 stages: the bootstrap & the second stage)
- A patition table


### booting steps

- POST
- Peripheral activation
- loading & executing bootstrap
- bootstrap loads the second stage of the bootloader


## UEFI
UEFI is a firmware that starts everytime a computer is powered on.
It relies on NVRAM, another non-volatile memory chip to find ESP applications (bootloader, diagnosis tools, etc...)
ESP applications are either automatically executed at boot or launched via a boot menu.

ESP applications must be stored on a conventional disk partition under a conventional filesystem (FAT12, FAT16, FAT32).

### booting steps

- POST
- Peripheral activation
- reads NVRAM address pointing to the ESP partition & executes the content (usually a BootLoader)
- the bootloader present boot options & run the chosen kernel & its parameters

!!! information
    UEFI brings a new functionality called "secure boot", designed to prevent any bootkit execution.  
    Its use is controversial as it also prevents any unrecognized ESP application to execute...


*[BIOS]: Basic Input/Output System
*[POST]: Power On Self Test
*[MBR]: A Master Boot Record is a boot sector, placed at the beginning of a disk. A boot sector contains machine code to be loaded in RAM so that a firmware can execute it. (512 bytes long)
*[UEFI]: Unified Extensible Firmware Interface is the first sector on a disk
*[firmware]: a program stored on a non-volatile memory chip, connected to the motherboard

*[bootstrap]: An executable code lauching the second stage of the bootloader.
*[second stage]: An executable code presenting boot options to the user, and loading the chosen kernel & parameters into RAM.

*[unrecognized ESP application]: Using secure boot, an ESP application can only be executed if its signature is sotred in NVRAM. Only the manufacturer can safely store signatures in NVRAM.

## GRUB
GRUB is the most popular boot loader for Linux.
Once called by BIOS or UEFI it displayes a list of entries, each representing an OS paired with a kernel and its parameters.

An entry can be edited to set another kernel and other parameters.

!!! example
    Here is the syntax of a GRUB entry
    ```bash
    
    ```

The following kernel parameters exist:

`mem=X`
sets the maximum amount of RAM used by the system (can be useful to limit VMs)

`maxcpus=X`
limit the number of processor (or core processor) used by the system

`quiet`
hides boot messages

`vga=ask`
select a video mode

`root=/dev/diskPartition`
define where the root partition is located

`rootflags`
define mount options for the root partition

`ro`
makes the root partition read-only

`rw`
allows read-write on the root partition

!!! warning
    Editing theses options via the command-line via GRUB will not persist modification and should be used
    for debugging purposes only.

!!! information
    To persist parameters across reboots.  
    Edit the `GRUB_CMDLINE_LINUX` line from the `/etc/default/grub` file, then generate a new configuration file for the bootloader using the `grub-mkconfig -o /boot/grub/grub.cfg` command.

!!! information
    current kernel parameters are visible in the `/proc/cmdline` file.


*[GRUB]: Grand Unified Bootloader
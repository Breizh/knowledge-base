# 101.1 Determine & configure hardware settings

## BIOS & UEFI
!!! info
    This document is a recap of LPIC-1 101.1  "How to interact with peripherals."


On most machines either BIOS or UEFI is installed.  
They are hardware configuration utilities installed on a motherboard chip  
We call this kind of independent software "Firmwares".

They both:

- ==identify== & ==connect== peripherals at each boot
- ==test peripherals health== at each boot
- ==provide a GUI== to tweak hardware settings ( ++f1++ .. ++f12++ to access it )

!!! info
    UEFI is BIOS's successor, it kinda does everything BIOS can and:
    
    - it supports more kind of hardware
    - it is developed in a Extensible way
    - it just performs better in most situations


## Debugging

### path
2 case scenario:

- hardware is not detected by the OS  
If BIOS config is OK, then the hardware may be defective.
- hardware is detected by the OS but not fully operational  
The hardware may require to tweak kernel settings.

---
### CLI utilities

`lspci`  
lists all hardware connected via PCI.

`lsusb`  
lists all hardware connected via USB.

`lsmod`  
shows all currently loaded kernel modules.  
|module_nam|RAM_space_taken|depending_modules

`modprobe -r <MODULE_NAME>`  
unloads the module `<MODULE_NAME>` if not used by a process.

`modinfo <MODULE_NAME>`  
Shows info concerning a kernel module.
you can then tweak settings by stating `options key=value` directives in the corresponding `/etc/modprobe.d/MODULE_NAME` file

`/etc/modprobe.d/blacklist.conf`  
Can be edited to prevent a kernel module from being loaded using the  
`blacklist MODULE_NAME` directive.

!!! info
    Good practice is to edit the `/etc/modprobe.d/MODULE_NAME` and leave the  `/etc/modprobe.conf` untouched.


### information files & device files

`/proc/cpuinfo`  
Lists detailed information about the CPU(s) found by the operating system.

`/proc/interrupts`  
A list of numbers of the interrupts per IO device for each CPU.

`/proc/ioports`  
Lists currently registered Input/Output port regions in use.

`/proc/dma`  
Lists the registered DMA (direct memory access) channels in use.


`/dev/`  
contains files representing devices.

`/sys/` contains information
contains files storing dynamic info related to the kernel and device information.

!!! info
    `/proc/` & `/sys/` are essentialy the same, the main difference being that `/proc/` lets you interact with processes.  
    `/sys/` lets you interact with the kernel or with the hardware as a whole. 

### storage devices

`/dev/`  
contains files representing devices.

In the past, files in the `/dev/` were named after the device's port type:

- SCSI
- SATA
- IDE
- USB
But now, all of them are named the same in the following order

first disk is `/dev/sda`:

- `/dev/sda1` 1 being the first partition.
- `/dev/sda...` more partitions
- `/dev/sdaX` X being the last partition.

second disk is `/dev/sdb`:

- `/dev/sdb1` 1 being the first partition.
- `/dev/sdb...` more partitions
- `/dev/sdbX` X being the last partition.

It goes on depending on how much disks are connected.

!!! information
    The only exception to this pattern are:

    - NVMe disk (SSD + PCI) Which are labelled `nvme<DN>n<NN>p<PN>` (e.g `nvme0n1p1`).
    - SSD cards via PCI bus which are labelled `/dev/mmcblk<CN>p<PN>` (e.g `/dev/mmcblk0p1`)

## external links
A list of numbers of the interrupts per IO device for each CPU.
BIOS & UEFI settings are covered in this [recap](../BIOS_UEFI.md)
*[BIOS]: Basic Input/Output System
*[UEFI]: Unified Extensible Firmware Interface
*[Firmwares]: Software running on a motherboard's chip, they can be used before the boot process as they are OS independent.
*[kernel]: Operating System :clown:
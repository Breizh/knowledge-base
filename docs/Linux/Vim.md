# Vim

`i` Enter "insert" mode to write text.

++esc++ or ++ctrl+c++  
Enter "normal" mode to navigate & to manipulate text.

## navigate
++k++ up   
++j++ down  
++l++ right  
++h++ left  

++w++ start of next word  
++b++ beggining of the word  
++e++ end of the word  
++f++ find a matching pattern  
% jump to matching parenthesis  

++0++ beginning of line  
$ end of line  

\* next same word  
\# previous same word  

`gg` beginning of file  
`G` end of file  
`XG` Go to line `X`  


!!! info
    You can combine number and letters such as 30i# ++esc++ which will write  
    `##############################`

## external link
https://www.openvim.com/
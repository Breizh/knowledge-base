# recap

| **Command** | **Description** |
| --------------|-------------------|
| `man <tool>` | Opens man pages for the specified tool. | 
| `<tool> -h` | Prints the help page of the tool. | 
| `apropos <keyword>` | Searches through man pages' descriptions for instances of a given keyword. | 
| `cat` | Concatenate and print files. |
| `whoami` | Displays current username. | 
| `id` | Returns users identity. | 
| `hostname` | Sets or prints the name of the current host system. | 
| `uname` | Prints operating system name. | 
| `pwd` | Returns working directory name. | 
| `ifconfig` | The `ifconfig` utility is used to assign or view an address to a network interface and/or configure network interface parameters. | 
| `ip` | Ip is a utility to show or manipulate routing, network devices, interfaces, and tunnels. | 
| `netstat` | Shows network status. | 
| `ss` | Another utility to investigate sockets. | 
| `ps` | Shows process status. | 
| `who` | Displays who is logged in. | 
| `env` | Prints environment or sets and executes a command. | 
| `lsblk` | Lists block devices. | 
| `lsusb` | Lists USB devices. | 
| `lsof` | Lists opened files. | 
| `lspci` | Lists PCI devices. | 
| `sudo` | Execute command as a different user. | 
| `su` | The `su` utility requests appropriate user credentials via PAM and switches to that user ID (the default user is the superuser).  A shell is then executed. | 
| `useradd` | Creates a new user or update default new user information. | 
| `userdel` | Deletes a user account and related files. |
| `usermod` | Modifies a user account. | 
| `addgroup` | Adds a group to the system. | 
| `delgroup` | Removes a group from the system. | 
| `passwd` | Changes user password. |
| `dpkg` | Install, remove and configure Debian-based packages. | 
| `apt` | High-level package management command-line utility. | 
| `aptitude` | Alternative to `apt`. | 
| `snap` | Install, remove and configure snap packages. |
| `gem` | Standard package manager for Ruby. | 
| `pip` | Standard package manager for Python. | 
| `git` | Revision control system command-line utility. | 
| `systemctl` | Command-line based service and systemd control manager. |
| `ps` | Prints a snapshot of the current processes. | 
| `journalctl` | Query the systemd journal. | 
| `kill` | Sends a signal to a process. | 
| `bg` | Puts a process into background. |
| `jobs` | Lists all processes that are running in the background. | 
| `fg` | Puts a process into the foreground. | 
| `curl` | Command-line utility to transfer data from or to a server. | 
| `wget` | An alternative to `curl` that downloads files from FTP or HTTP(s) server. |
| `python3 -m http.server` | Starts a Python3 web server on TCP port 8000. | 
| `ls` | Lists directory contents. | 
| `cd` | Changes the directory. |
| `clear` | Clears the terminal. | 
| `touch` | Creates an empty file. |
| `mkdir` | Creates a directory. | 
| `tree` | Lists the contents of a directory recursively. |
| `mv` | Move or rename files or directories. | 
| `cp` | Copy files or directories. |
| `nano` | Terminal based text editor. | 
| `which` | Returns the path to a file or link. |
| `updatedb` | Updates the locale database for existing contents on the system. |
| `locate` | Uses the locale database to find contents on the system. | 
| `find` | Searches for files in a directory hierarchy. | 
| `head` | Prints the first ten lines of STDOUT or a file. |
| `tail` | Prints the last ten lines of STDOUT or a file. | 
| `more` | Pager that is used to read STDOUT or files. |
| `less` | An alternative to `more` with more features. | 
| `grep` | Searches for specific results that contain given patterns. | 
| `sort` | Sorts the contents of STDOUT or a file. |
| `cut` | Removes sections from each line of files. |
| `tr` | Replaces certain characters. | 
| `column` | Command-line based utility that formats its input into multiple columns. |
| `awk` | Pattern scanning and processing language. |
| `sed` | A stream editor for filtering and transforming text. | 
| `wc` | Prints newline, word, and byte counts for a given input. |
| `chmod` | Changes permission of a file or directory. |
| `chown` | Changes the owner and group of a file or directory. |
| `file <FILE>` | Prints file format |

# find
```bash
find / -type f -name "*.conf" -user root -size -28k +25k -exec ls -al {} \; 2> error.log 1> output.log 
```

# STDIN STDERR STDOUT

## EOF marker
Whenever an input is required, you can write multiple lines by calling EOF
```bash
# STDIN goes in EOF, then cat. The output then goes in stream.txt, errors in /dev/null/
cat << EOF > stream.txt 2> /dev/null/
this
is a text
EOF
```
```bash
cat stream.txt
#returns:
this
is a text
```

## pipes
When using bash, the standart output is the shell, but we can redirect the ouput using pipes `|`.
It can be useful when you need to redirect output from a program to another.

!!! To-Do
    create a cheat-sheet for dpkg

## dpkg

`dpkg -l`

lists packages `ii` means currently installed `rc` means package removed but configuration files still installed.

## pagers
- head
- tail
- more
- less

## filter STDOUT
```bash
cat file.txt | tr "<TO_REPLACE_CHAR" "<REPLACING_CHAR>"
cat file.txt | grep -i <REGEX> > file.txt
cat file.txt | cut -d ":" -f<LINE_NUMBER> > filtered_column.txt
cat file.txt | sort > sorted_file.txt
```

## awk
```bash
cat file.txt |awk '{print $1, $NF}'
cat file.txt |awk '{print $6, $1}'
```

## sed
sed is a bit different, it allows to modify occurences and more, filtering is not its only purpose
```bash
```
## To Do
separated cheat-sheet or section for:
- awk
- grep
- dpkg
- ss

and link each overview section from this file to the dedicated cheat-sheet


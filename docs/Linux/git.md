# Git

## installation
```
sudo apt-get install git
```

## profile configuration
```
git config --global user.name "ralsei"
```

```
git config --global user.email "ralsei@gmail.com"
```

## common usage
```
git add <filename1|filename2|...>
```

```bash
git commit -m "a message giving out context on the commited files"
--amend

```

```
git pull
```

```
git push
```

## advanced usage

```
git merge
```

```
git rebase
git rebase -i HEAD~3
```

```
cherry pick #TODO
```

## debugging

```
git status
```

```
git diff
git diff filename
git diff branch1 branch2
```
# iptables

iptables is an utility letting you interact with Netfilter (a fire-wall built into the Linux kernel)
When a packet is received:

- iptables matches the packet with a table (FILTER, NAT, MANGLE, SECURITY)
- iptables matches the packet with a chain (INPUT, OUTPUT, FORWARD, PREROUTING, POSTROUTING, ...)
- iptables matches the packet with a rule (by comparing the rule's flags and the packet)
- the rule specify what iptables will do, (drop the packet, accept it, etc)

## set default policies
```bash
iptables --policies INPUT <ACCEPT|DROP>
iptables --policies OUTPUT <ACCEPT|DROP>
iptables --policies FORWARD <ACCEPT|DROP>
```

## flush current rules & chains
```bash
iptables --flush --delete-chain
```
!!! danger
    DO NOT flush rules if your policies are DROPPING packets as you will loose  
    remote access to the server !

## list rules
```bash
iptables -L
```

## persist rules across reboot
```bash
iptables–save
```

## rule examples
```bash
#a general example
iptables [<-t table>] --<append|insert|delete> <INPUT|OUTPUT|FORWARD>  <FLAGS> -j <ACCEPT|DROP>
#accepting packet if: port is 12345 protocol is TCP
iptables -t filter --append INPUT --protocol tcp --dport 12345 -j ACCEPT
#omitting the table is the same as specifying the 'filter' table
iptables --append INPUT --protocol tcp --dport 12345 -j ACCEPT
```
!!! hint
    For more flags, read the "PARAMETER" section of iptables's man page
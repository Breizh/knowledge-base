# storage

## partition
### what is it
A partition is a logical division of a hard storage device (SSD, HDD, USB, ...)  
Each partition is seen as a storage device by an OS.

partitions are used to separate critical information to the rest.
!!! example
    The operating system is in its own partition

!!! example
    Filling an entire storage may leave a system unusable.  
    placing `/var/log/` in a separate partition protects the storage from being entirely filled-up.



2 main types of partition exist:

### Master Boot Record

!!! info
    The MBR specification is part of the BIOS specification.

!!! info
    4 partitions max for a hard drive, 2TB max by partition



The MBR sector (first disk sector) contains:

- the bootloader
- the partition table

When a system is powered on:

- the [BIOS](#) will read the first sector find the bootloader, place it into RAM, execute it.
- the [bootloader](#) places the OS in the RAM

### GUID Partition Table
!!! info
    The GPT specification is part of the UEFI specification.

!!! info
    128 partitions max for a hard drive, 9.4ZB max for each partition

The protective-MBR sector (first disk sector) contains the bootloader for compatibility reasons.  
Then follows the GPT header & GPT partition table.

A specific partition named ESP must contain any booting services  
(bootloader, diagnostic tools, etc...)


When a system is powered on:

- The [UEFI] looks for a bootloader in the [ESP]() partition & loads it into RAM
- The bootloader places the OS into the RAM

!!! info
    UEFI-based firmware gets the booting configuration from  NVRAM  (a non-volatile RAM).  
    The NVRAM contains information such as the boot settings & the bootloader path

## Filesystem
A filesystem is a set of data structures, interfaces, APIs, and abstractions working together to  
manage files on a storage device.

Without a fs, data would not be usable on a storage, the OS would not know how to handle data

!!! example
    Here is a list of widely used filesystems across Windows & Linux OS:  
    **NTFS, FAT32, Ext4, NFS, SMB**

### architecture

*[GUID]: Global Unique ID
*[GPT]: GUID Partition Table
*[MBR]: Master Boot Record
*[BIOS]: Basic Input/Output System
*[ESP]: EFI System Partition

## external documentation
- an amazing take on partitions, fs, booting, and more [here](https://www.freecodecamp.org/news/file-systems-architecture-explained/)
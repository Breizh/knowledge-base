# systemd

a simple cheat-sheet concerning service creation & interaction

---

service are located under the `/etc/systemd/system/` directory and suffixed `.service`.

example below
```ini
#/etc/systemd/system/SERVICE_NAME.service
[Unit]
Description= A sentence to explain what this service is about
#you can start other services before or after this one if there is dependencies
After=<target|service>
Before=<target|service>

[Service]
#what systemctl start SERVICE_NAME is mapped to
ExecStart=<COMMAND>
#what systemctl reload SERVICE_NAME is mapped to
ExecReload=<COMMAND>
#you can set variables in a separate file
EnvironmentFile=<PATH>

#concerning systemctl enable/disable
[Install]
#The concerned "target" on which enable/disable command will focus
#target <~> systemd's run-level
WantedBy=multi-user.target
```

!!! hint
    many optional parameters, read the official doc for more information

service interactions
```bash
systemctl <start|stop|restart|reload| SERVICE_NAME.service
```
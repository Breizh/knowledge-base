# PAM
Pluggable Authentication Modules.  
Configuration at `/etc/pam.conf`, `/etc/pam.d/`

!!! info
    PAM may be installed by default on Linux distros, but you must install PAM modules based on your needs.  
    PAM modules extend the basic usage of PAM.
!!! example
    If you need to authenticate users relying on an LDAP server,
    you can install the `libpam-ldap` package.

---

## Introduction
PAM configuration files are nothing more than rulesets.  
To define a rule, you must specify:

- a [module type](#module-types)
- a [control type](#control-types)
- the concerned [library](#library)

!!! example
    The request account must be available, verification is made relying on the pam-unix package
    ```bash
    module: control:   library:
    account	required   pam_unix.so # pam_unix.so <=> checking /etc/passwd & /etc/shadow
    ```
!!! info
    modules types and control types are the same among PAM libraries. But each PAM library has its own back-end implementation, `man <MODULE_TYPE>` for more information.
---

## module types
!!! info
    each module types consist of at least one or more functionalities which can be employed.


`account`  
Ensure that the requested account is available (account expiration, valid datetime)

`auth`  
Ensure the credentials are correct.  
Grant permissions related to the user (group membership, ...)

`password`  
Lets the user update their credentials (explicit request, expiricy, ...)

`session`  
Sets up the user environment, and unset it once the user is disconnected (e.g mnt homedir and volumes)

---

## control types

### 1st syntax

`required`  
if the rule fails, the authentication fails, but first the following rules are processed.

`requisite`  
if the rule fails, the authentication abruptly fails.

`sufficient`  
if the rule fails, the following rules are read.  
if the rule succeeds, the authentication is instantly granted to the user.

`optional`  
Failure or success does not have consequences EXCEPT if this is the only rule targeting the given (service, module_type) pair.

`include`  
TODO

`substack`  
TODO

### 2nd syntax

You can also rely on the "square brackets" syntax if control keywords do no match your use case.  
!!! example
    ```
    module_type [value1=action1 value2=action2 ...] library
    auth    [success=1 default=ignore pam_sss.so use_first_pass]
    ```


=== "return values"
    A module type returns one of the following values (valueN):
    ```
    success, open_err, symbol_err, service_err, system_err, buf_err, perm_denied, auth_err, cred_insufficient,
    authinfo_unavail, user_unknown, maxtries, new_authtok_reqd, acct_expired, session_err, cred_unavail, cred_expired, cred_err,
    no_module_data, conv_err, authtok_err, authtok_recover_err, authtok_lock_busy, authtok_disable_aging, try_again, ignore, abort,
    authtok_expired, module_unknown, bad_item, conv_again, incomplete, and default
    ```
=== "action values"
    The actionN can take one of the following forms:

    `ignore`  
    when used with a stack of modules, the module's return status will not contribute to the return code the application obtains.

    `bad`  
    this action indicates that the return code should be thought of as indicative of the module failing. If this module is the
    first in the stack to fail, its status value will be used for that of the whole stack.

    `die`  
    equivalent to bad with the side effect of terminating the module stack and PAM immediately returning to the application.

    `ok`  
    this tells PAM that the administrator thinks this return code should contribute directly to the return code of the full stack
    of modules. In other words, if the former state of the stack would lead to a return of PAM_SUCCESS, the module's return code
    will override this value. Note, if the former state of the stack holds some value that is indicative of a modules failure,
    this 'ok' value will not be used to override that value.

    `done`  
    equivalent to ok with the side effect of terminating the module stack and PAM immediately returning to the application.

    `N`   (an unsigned integer)
    jump over the next N modules in the stack. 
    
    !!! warning
        Note that N equal to 0 is not allowed, it would be treated as ignore in such case.
        The side effect depends on the PAM function call: for pam_authenticate, pam_acct_mgmt, pam_chauthtok, and pam_open_session it is ignore; for pam_setcred and pam_close_session it is one of ignore, ok, or bad depending on the module's return value.

---
!!! example
    Here is a complete example
    ```bash
    #if local authentication ok, jump 2 next rules, thus avoiding pam_deny.so
    auth    [success=2 default=ignore]      pam_unix.so nullok
    #else if sssd authentication ok, jump the next rule, thus avoiding pam_deny.so
    auth    [success=1 default=ignore]      pam_sss.so use_first_pass
    auth    requisite                       pam_deny.so
    auth    required                        pam_permit.so
    auth    optional                        pam_mount.so 
    ```
!!! info
    The `default` value is a fallback in case none of the specified values are matched.
# To Do
## priority
improve IG role & doc here:

- [ ] sudoers
- [ ] SSH
- [ ] NFS
- [ ] samba
- [ ] SSSD
- [ ] pam
- [ ] timesyncd
- [ ] postfix
- [ ] ftp
- [ ] fail2ban
- [ ] resolv
- [ ] CUPS
- [ ] apt
- [ ] dpkg
- [ ] Linux :clown:

## authentication
- [ ] LDAP
- [ ] sssd
- [ ] Kerberos
- [ ] CAS
- [ ] nsswitch

## ressources sharing
- [x] NFS (SMB dialecte Linux <--> Linux) (mountd, statd, lock.d, etc...)
- [ ] CIFS (SMB dialecte for Linux & Windows in both sides)
- [ ] SAMBA (SMB dialecte for windows client <--> Linux server)


## Web servers / proxies
- [ ] Nginx
- [ ] Apache

## Devops
- [ ] Bash
- [ ] Python
- [ ] Ansible
- [ ] Gitlab CI
- [ ] Flask

## Database administration
- [ ] PostgreSQL
- [ ] SQLite

## Frameworks
- [x] Flask

## Networking & security
- [x] iptables
- [x] GnuPG
- [ ] DNS
- [ ] DHCP
- [ ] VPN
- [ ] SDN
- [ ] Cisco ACI

## Hardware

## Other
- [ ] Vim
- [ ] LPIC-1

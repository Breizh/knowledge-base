# rsyslog

Rsyslog is a utility used to send logs through TCP/UDP comunications.  
From a client to a server.

Configuration here:

- `/etc/rsyslog.conf`
- `/etc/rsyslog.conf.d/*`

## client-side
```bash
sudo apt-get install rsyslog
sudo systemctl restart rsyslog
```

In the configuration file, you can map a facility & a priority to a log file
```conf
FACILITY.PRIORITY -/PATH/TO/LOCAL/FILE
```

!!! hint
    The `-` states that the logs will be written locally.


Here is a brief list of existing facilities:

- kern
- auth
- authpriv
- mail
- daemon

Here is a brief list of existing priority:

- info
- debug
- warn
- err

### local logging

To log every kernel warnings to the `/var/log/kern.log` file
```bash
kern.warn -/var/log/kern.log
```

To log any log having a `DEBUG` priority to the `/var/log/debug`
```bash
*.debug -/var/log/debug.log
```

You can also mix facilities & priorities in a single statement
```bash
kern.err;auth.err;daemon.err -/PATH/TO/LOCAL/FILE
```
A shorter way to put it
```bash
kern,auth,daemon.err -/PATH/TO/LOCAL/FILE
```

!!! warning
    Do not mistake `;` which states a separation between a FACILITY.PRIORITY tuple  
    and `,` which lets you state many FACILITY at once, for a same PRIORITY which you must set
    at the end of the statement.

### remote logging

To send logs to a log server
```bash
@@[(zNUMBER)]HOST:[PORT] #NUMBER being the compression level #@@ <=> TCP
@[(zNUMBER)]HOST:[PORT] #NUMBER being the compression level #@ <=> UDP
```

```bash
# redirect all logs to 192.168.1.1 using UDP and zlib compression lvl 2
*.* @(z2)192.168.0.1
# redirect all logs to 192.168.1.1 using TCP
*.* @@192.168.0.1 
```

## server-side
```bash
sudo apt-get install rsyslog
sudo systemctl enable rsyslog
```

Then specify to rsyslog that it must run in server mode
```bash
#/etc/rsyslog.conf

# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")

# provides TCP syslog reception
module(load="imtcp")
input(type="imtcp" port="514")
```

specify where to store incoming logs
```bash
#/etc/rsyslog.conf

$template remoteLogs,"/var/log/%HOSTNAME%/%PROGRAMNAME%.log"
*.* ?remoteLogs
& ~
```
!!!info
    `& ~` tells rsyslog to not process incoming logs once written to a file,  
    else logs would be stored in the template remoteLogs and in its own log files

restart the service
```bash
sudo systemctl restart rsyslog
```

## external resources

[Red Hat's documentation](https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/s1-basic_configuration_of_rsyslog) on the topic.
# DNS
!!! information
        [DNS insane guide](https://www.zytrax.com/books/dns/)

## Introduction
The Domain Name System translates a Domain Name to an IP address
!!! example
    DNS(archlinux.org) => 95.217.163.246

Web browsers needs ip addresses to request web pages,  but it is easier for humans  
to remember words than ip addresses.

DNS has been created to ease the process of reaching out to a server.

## recap

![Domain Name Structure Image](../img/DNS_structure.excalidraw.svg)


---

![Domain Name Structure Image](../img/DNS_resolution.excalidraw.svg)